import tkinter as tk

class Conversor():
	def __init__(self,window):
		self.window = window
		self.window.title("Conversor binario")
		self.window.geometry("300x200")
		self.window["bg"] = ""
		self.frame1 = tk.Frame(self.window,bg="darkblue")
		self.frame2 = tk.Frame(self.window,bg="darkblue")
		
		self.bt_ok = tk.Button(self.frame1,text="OK",width=7,height=3,fg="white",bg="black",command=self.converter_bin)
		self.bt_ok.pack(side="bottom")

		self.lb_saida = tk.Label(self.frame2,text="Saida",bg="darkblue",fg="white",width=30,height=4)
		self.lb_saida.pack(side="top")

		self.et_entrada = tk.Entry(self.frame1,bg="black",fg="white")
		self.et_entrada.pack(side="top")

		self.frame1.pack(side="bottom")
		self.frame2.pack(side="top")

	def converter_bin(self):
		try:
			self.lb_saida["text"] = str(format(int(self.et_entrada.get()),"b"))
			self.lb_saida["fg"] = "white"
		except:
			self.lb_saida["text"] = "Somente numeros inteiros!"
			self.lb_saida["fg"] = "red"

window = tk.Tk()
Conversor = Conversor(window)
window.mainloop()